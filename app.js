const express = require("express");
const app = express();
const Product = require("./models/product");
const Order = require("./models/order");
const bodyParser = require("body-parser");
const path = require("path");
const mongoose = require("mongoose");

app.set("view engine", "ejs");
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(express.static(path.join(__dirname, "views")));

app.get("/delivery", (req, res) => {
  res.render("delivery.ejs");
});
app.get("/payment", (req, res) => {
  res.render("payment.ejs");
});
app.get("/add_product", (req, res) => {
  res.render("add_product.ejs");
});
app.get("/product/:product_id", (req, res) => {
  Product.findOne({
    _id: mongoose.Types.ObjectId.createFromHexString(
      req.params.product_id.toString()
    ),
  }).then((product) => {
    res.render("product_page.ejs", { product: product });
  });
});
app.get("/proceed/:product_id", (req, res) => {
  Product.findOne({
    _id: mongoose.Types.ObjectId.createFromHexString(
      req.params.product_id.toString()
    ),
  }).then((product) => {
    res.render("proceed.ejs", { product: product });
  });
});
app.post("/add_product", (req, res) => {
  const { name_of_product, image, price } = req.body;

  Product.create({
    name_of_product: name_of_product,
    image: image,
    price: price,
  }).then((product) => console.log(product._id));

  res.redirect("/");
});
app.post("/orders", (req, res) => {
  const { phone_number } = req.body;
  res.redirect("/orders/" + phone_number);
});
app.get("/orders/:phone_number", (req, res) => {
  Order.find({}).then((orders) => {
    const filtered = orders.filter((order) => {
      return order.phone === req.params.phone_number;
    });
    res.render("orders.ejs", { orders: filtered });
  });
});
app.get("/orders", (req, res) => {
  console.log({});
  res.render("orders.ejs", { orders: [] });
});
app.post("/order/:product_id", (req, res) => {
  const { phone_number } = req.body;
  Product.findOne({
    _id: mongoose.Types.ObjectId.createFromHexString(
      req.params.product_id.toString()
    ),
  }).then((product) => {
    Order.create({
      phone: phone_number,
      name_of_product: product.name_of_product,
      price: product.price,
    }).then((order) => {
      const phone_number = encodeURIComponent(order.phone);
      res.redirect("../orders/" + phone_number);
    });
  });
});
app.get("/", (req, res) => {
  Product.find({}).then((products) => {
    res.render("index.ejs", { products: products });
  });
});

module.exports = app;
