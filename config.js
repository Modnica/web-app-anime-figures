module.exports = {
  PORT: process.env.PORT || 8080,
  MONGO_URL: "mongodb://localhost/anime_store",
};
