const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const schema = new Schema({
  name_of_product: {
    type: String,
    required: true,
  },
  image: {
    type: String,
    required: true,
  },
  price: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("Product", schema);
