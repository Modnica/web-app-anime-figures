const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const schema = new Schema({
  phone: {
    type: String,
    required: true,
  },
  name_of_product: {
    type: String,
    required: true,
  },
  price: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("Order", schema);
